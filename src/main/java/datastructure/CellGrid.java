package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private final int rows;
    private final int columns;
    private CellState grid[][];
    private CellState initialState;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.grid = new CellState[rows+1][columns+1];
        for (int i = 0; i < this.rows+1; i++) {
            for (int j = 0; j < this.columns+1; j++) {
                grid[i][j] = initialState;
            }

        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if(0 <= row && row < rows){
            if(0 <= column && column < columns){
                grid[row][column] = element;
            } else{
                throw new IndexOutOfBoundsException();
            }
        } else{
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public CellState get(int row, int column) {
        if(0 <= row && row <= rows){
            if(0 <= column && column <= columns){
                return grid[row][column];
            } else{
                throw new IndexOutOfBoundsException();
            }
        }
        else{
            throw new IndexOutOfBoundsException();
        }

    }

    @Override
    public IGrid copy() {
        IGrid grid = new CellGrid(rows, columns, initialState);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                grid.set(i,j, this.grid[i][j]);
            }
        }
        return grid;
    }
    
}
