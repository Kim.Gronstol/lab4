package gui;

import cellular.BriansBrain;
import cellular.CellAutomaton;
import cellular.GameOfLife;

public class Main {

	public static void main(String[] args) {
		CellAutomaton ca = new GameOfLife(800,800);
		//CellAutomaton ca = new BriansBrain(1000, 1000);
		CellAutomataGUI.run(ca);
	}

}
